<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    /**
     * 檢視圖片
     */
    public function show($id) {
        if( ! Str::isUuid($id) ) {
            return abort(404);
        }

        $image = Image::findOrFail($id);

        // 在瀏覽器中直接顯示圖片
        $path = storage_path('app/' . $image->file_path);
        return response()->file($path);
    }

    /**
     * 新增圖片
     *
     * @return json
     */

    public function store(Request $request) {
        $requestImage = $request->file('image');

        // 檢查檔案類型是否爲圖片
        if( ! in_array($requestImage->getMimeType(), \App\Image::ACCEPTED_MINE_TYPES) ) {
            return  response()->json([
                'status'        => 'upload file fails',
                'error_code'    => $requestImage->getError(),
                'error_message' => 'uploaded file is not an image.',
            ]);
        }

        // 如果上傳失敗，回傳錯誤訊息
        if( ! $requestImage->isValid()) {
            return  response()->json([
                'status'        => 'upload file fails',
                'error_code'    => $requestImage->getError(),
                'error_message' => $requestImage->getErrorMessage(),
            ]);
        }

        $id = (string) Str::orderedUuid();
        DB::transaction(function () use ($requestImage, $id) {
            $now = now();
            $path = sprintf('images/%d/%02d', $now->year, $now->month);

            $image = new Image;
            // 主鍵使用有時間順序的 UUID ，搜尋會比較快
            $image->id = $id;
            $image->file_path = $requestImage->store($path);
            // 客戶端原始檔名
            $image->original_name = $requestImage->getClientOriginalName();
            $image->save();
        });

        return response()->json([
            'status'       => 'upload file successfully',
            'originalName' => $requestImage->getClientOriginalName(),
            'id'           => $id, // UUID
        ]);
    }
}
