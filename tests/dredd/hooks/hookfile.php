<?php

use Dredd\Hooks;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Artisan;

require __DIR__ . '/../../../vendor/autoload.php';

$app = require __DIR__ . '/../../../bootstrap/app.php';

$app->make(Kernel::class)->bootstrap();

Hooks::beforeAll(function (&$transaction) use ($app) {
    # 開始之前，先清空資料庫，回到初始值：只有說明 Markdown 語法的條目和使用者角色清單
    Artisan::call('migrate:fresh', [ '--force' => true, '--seed' => true ]);
});
