<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('articles.')->prefix('articles')->group(function() {
    Route::get('create',                'ArticleController@create')->name('create');
    Route::post('/',                    'ArticleController@store')->name('store');
    Route::get('{title}',               'ArticleController@show')->name('show');
    Route::get('{title}/edit',          'ArticleController@edit')->name('edit');
    Route::put('{title}',               'ArticleController@update')->name('update');
    Route::patch('auth/{user}',         'ArticleController@auth')->name('auth');
});
Route::post('preview',                  'ArticleController@preview');
Route::get('search/articles',           'ArticleController@search')->name('articles.search');

Route::get('images/{image}',            'ImageController@show')->name('images.show');
Route::post('images',                   'ImageController@store')->name('images.store');

Route::get('login',                     'Auth\LoginController@showLoginForm')->name('login');
Route::post('logout',                   'Auth\LoginController@logout')->name('logout');
Route::get('login/{provider}',          'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('user/auth',                 'UserController@auth')->name('user.auth');
Route::put('user/auth/{user}',          'UserController@changeRole');

Route::redirect('/', 'articles/home')->name('home');
Route::redirect('/home', 'articles/home');
