@extends('layouts.app')

@section('title', 'Search ' . $keyword)

@section('content')
@inject('markdown', 'App\Markdown\MarkdownService')
@php use Illuminate\Support\Str; @endphp

@if($articles->count() == 0)
    @include('articles.partials.search-not-found')
@else
    @include('articles.partials.search-result')

    @include('articles.partials.search-result-pagination')
@endif
@endsection
