<div class="form-group ml-auto">
    <select class="form-control" id="article-auth">
        @foreach($options as $option)
            <option value="{{ $option['id'] }}"{{ $option['selected'] ?? '' }}>
            {{ $option['name'] }}
            </option>
        @endforeach
    </select>
    <label for="article-auth" class="ml-2">can update</label>
</div>
