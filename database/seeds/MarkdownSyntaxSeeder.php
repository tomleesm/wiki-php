<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Image;;
use Illuminate\Support\Str;

class MarkdownSyntaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 新增條目 Markdown Syntax
        $path = __DIR__ . '/markdown-syntax.md';

        $article          = new Article();
        $article->title   = 'Markdown Syntax';
        $article->content = file_get_contents($path);
        $article->is_restricted = true;
        $article->role_id = Role::ADMINISTRATOR;
        $article->save();

        // 新增條目 include:中高運量鐵路系統
        $path = __DIR__ . '/include.md';

        $article          = new Article();
        $article->title   = 'include:中高運量鐵路系統';
        $article->content = file_get_contents($path);
        $article->is_restricted = true;
        $article->role_id = Role::ADMINISTRATOR;
        $article->save();

        // 新增內含的圖片
        DB::transaction(function () {
            $image = new Image;
            $image->id = '91ffa9b4-7872-4231-9e73-6066b7af09ca';

            $now = now();
            $path = sprintf('images/%d/%02d', $now->year, $now->month);
            // 把 Markdown-mark.png 複製到上傳檔案的目錄
            if( ! file_exists(storage_path('app/' . $path))) {
                mkdir(storage_path('app/' . $path), 0777, true);
            }
            copy(__DIR__ . '/Markdown-mark.png', storage_path('app/' . $path . '/125dda896f2af1357c37e5db33807d1dc46d9298.png'));
            $image->file_path = $path . '/125dda896f2af1357c37e5db33807d1dc46d9298.png';

            // 客戶端原始檔名
            $image->original_name = 'Markdown-mark.png';
            $image->save();
        });
    }
}
