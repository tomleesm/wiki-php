<p>The article <a href="{{ route('articles.show', ['title' => $keyword]) }}">{{ $keyword }}</a> does not exist. You can create it.</p>
<p>There were no results matching the query.</p>
</p>
