<div class="content markdown-body col-12 @unless($article->toc == '') col-lg-10 @endunless">
{{-- 如果 $article->body 有縮排，會造成 markdown 轉 html 的第一行變成 <pre> --}}
{!! $article->body !!}
</div>
