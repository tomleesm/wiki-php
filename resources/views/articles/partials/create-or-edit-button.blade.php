@if($article->exist)
    <a class="btn btn-success" href="{{ route('articles.edit', [ 'title' => $article->title ]) }}" role="button">Edit</a>
@else
    <a class="btn btn-success" href="{{ route('articles.create') }}" role="button">Create</a>
@endif
