<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * 可接受的檔案類型
     */
    public const ACCEPTED_MINE_TYPES = ['image/apng', 'image/bmp', 'image/gif', 'image/x-icon', 'image/jpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/webp'];

    public $incrementing = false;
    protected $keyType = 'string';
}
