@extends('layouts.app')

@section('content')
<div class="row title">
    <h1>{{ $article->title }}</h1>

    <div class="ml-auto mt-2">
        @include('articles.partials.create-or-edit-button', [ 'article' => $article ])
    </div>
</div>

@if( ! $article->exist && $article->title == 'home' )
    @include('articles.partials.default-blank')
@else
<div class="row">
@include('articles.partials.markdown-render-result', [ 'article' => $article ])
@include('articles.partials.toc', [ 'article' => $article ])
</div>
@endif
@endsection

@push('javascript')
    <script src="{{ asset('js/prism.js')}}" defer></script>
@endpush
