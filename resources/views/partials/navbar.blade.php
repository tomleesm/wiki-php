<nav class="navbar navbar-dark bg-dark navbar-expand-md mb-2">
    @include('partials.logo')
    @include('partials.search')

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Authentication Links -->
        <ul class="navbar-nav ml-auto">
            @guest
                @include('partials.login')
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @include('partials.authorization-link')
                        @include('partials.logout')
                    </div>
                </li>
            @endguest
        </ul>

    </div>
</nav>
