<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', $article->title ?? '') - {{ env('APP_NAME', 'wiki.php') }}</title>
        <link rel="stylesheet" href="{{  asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{  asset('css/simplemde.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css')}}" type="text/css">
        <link rel="stylesheet" href="{{ asset('css/prism.css')}}" type="text/css">
    </head>
    <body class="line-numbers bg-white">
        @include('partials.navbar')
        <div class="container">
            @yield('content')
        </div>
    </body>
    <script src="{{ asset('js/app.js') }}" defer></script>
    @stack('javascript')
</html>
