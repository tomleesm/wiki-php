@unless($article->toc == '')
{{-- 寬度 lg 以上才顯示目錄 --}}
<div class="toc d-none d-lg-block col-lg-2 ml-auto">
{!! $article->toc !!}
</div>
@endunless
