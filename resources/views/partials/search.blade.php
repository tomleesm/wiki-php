<!-- search -->
<form class="form-inline my-2 my-lg-0" action="{{ route('articles.search') }}" method="get">
  <input name="keyword" class="form-control mr-sm-2" type="search" value="{{ $keyword ?? '' }}" placeholder="Search articles" aria-label="Search articles">
  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
</form>
